using System;
using System.Linq;
namespace ejercicio_04_master
{
    class Ejercicio4
    {
        static void Main(string[] args)
        {
          /*Un programa que pida al usuario 10 números y
           luego calcule y muestre cuál es el mayor de todos ellos.*/

            int[] valores = new int[10]; 

         valores[0] = 43;
         valores[1] = 17;
         valores[2] = 55;
         valores[3] = 20;
         valores[4] = 80;
         valores[5] = 32;
         valores[6] = 7;
         valores[7] = 2;
         valores[8] = 9;
         valores[9] = 72;

         Console.WriteLine(valores.Max());

        }
    }
}
