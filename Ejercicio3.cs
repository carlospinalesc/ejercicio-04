using System;
namespace ejercicio_04_master
{
    class Ejercicio3
    {
        static void Main(string[] args)
        {
          /*Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto),
           pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.*/

           Console.WriteLine("Ingrese un numero del 1 al 12");

           int[] Dias = new int[4];
           Dias[1] = 31;
           Dias[2] = 30;
           Dias[3] = 28;

           int[] Meses = new int[13];
           Meses[1] = Dias[1];
           Meses[2] = Dias[3];
           Meses[3] = Dias[1];
           Meses[4] = Dias[2];
           Meses[5] = Dias[1];
           Meses[6] = Dias[2];
           Meses[7] = Dias[1];
           Meses[8] = Dias[1];
           Meses[9] = Dias[2];
           Meses[10] = Dias[1];
           Meses[11] = Dias[2];
           Meses[12] = Dias[1];

           int Mes = 0;
           Mes = Convert.ToInt32(Console.ReadLine());
           Console.WriteLine("El mes número {0} tiene {1} dias", Mes, Meses[Mes]);
        }
    }
}